    import java.util.ArrayList;
    import java.util.List;
    import java.util.ListIterator;

    class ListIteratorExample {
        public static void main(String[] args) {
            List<String> fruits = new ArrayList<>();
            fruits.add("Apple");
            fruits.add("Banana");
            fruits.add("Orange");
            
            ListIterator<String> iterator = fruits.listIterator();
            
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
            
            while (iterator.hasPrevious()) {
                System.out.println(iterator.previous());
            }
        }
    }

