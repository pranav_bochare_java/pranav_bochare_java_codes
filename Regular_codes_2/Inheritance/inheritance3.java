class parent{
	int x=10;
	parent(){
		System.out.println("In Parent Constructor");
	}
	void acess(){
		System.out.println("Parent instance");
	}

}
class child extends parent{
	int y=20;
	child(){
		System.out.println("In child constructor");
		System.out.println(x);
		System.out.println(y);
	}
}
class client{
	public static void main(String[] args){
		child obj=new child();
		obj.acess();
	}
}
