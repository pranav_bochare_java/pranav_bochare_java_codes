class parent{
	parent(){
		System.out.println("In parent constructor");
	}
	void parentproperty(){
		System.out.println("Flat,car,Gold");
	}
}
class child extends parent{
	child(){
		System.out.println("In child constructor");
	}

}
class client{
	public static void main(String[] args){
		child obj2 = new child();
		obj2.parentproperty();
	}
}
