class Parent{
	static void fun(){
		System.out.println("In parent fun");
	}
}
class child extends Parent{
	static void fun(){
		System.out.println("In child fun");
	}
}
class client{
	public static void main(String[] args){
		Parent obj=new Parent();
		obj.fun();
		child obj2=new child();
		obj2.fun();
		Parent obj3=new child();
		obj3.fun();
	}
}
