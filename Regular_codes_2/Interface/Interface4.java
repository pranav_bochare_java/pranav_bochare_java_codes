interface Demo1{
	default void fun(){
		System.out.println("IN Fun-Demo");
	}
}
interface Demo2{
	default void fun(){
		System.out.println("In Fun-Demo2");
	}
}
class Demochild implements Demo1,Demo2{
	public void fun(){
		System.out.println("In fun-Demo-child");
	}
}
class client{
	public static void main(String[] args){
		Demochild obj=new Demochild();
		obj.fun();
		Demo1 obj1=new Demochild();
		obj1.fun();
		Demo2 obj3=new Demochild();
		obj3.fun();
	}
}
