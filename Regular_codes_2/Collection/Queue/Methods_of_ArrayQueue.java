 class Calculator {

    public static final int inc(int a, int b) {
        return a + b;
    }

    public static final int dec(int a, int b) {
        return a - b;
    }

    public static final int inc_by_n(int a, int n) {
        return a + n;
    }

    public static final int sub(int a, int b, int c) {
        return a - b - c;
    }

    public static final <E> E elementAt(E[] array, int index) {
        return array[index];
    }

    public static final <E> E nonNullElementAt(E[] array, int index) {
        if (array[index] != null) {
            return array[index];
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(inc(1, 2)); // 3
        System.out.println(dec(10, 5)); // 5
        System.out.println(inc_by_n(10, 2)); // 12
        System.out.println(sub(10, 5, 2)); // 3
        System.out.println(elementAt(new Integer[]{1, 2, 3}, 0)); // 1
        System.out.println(nonNullElementAt(new Integer[]{1, null, 3}, 1)); // null
    }
}
