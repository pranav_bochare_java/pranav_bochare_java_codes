import java.util.*;
class TreeMapDemo{
	public static void main(String[] args){
		SortedMap tm=new TreeMap();
		tm.put("IND","INDIA");
		tm.put("PAK","Pakistan");
		tm.put("SL","ShriLanka");
		tm.put("AUS","Australia");
		tm.put("BAN","BanglaDesh");
		System.out.println(tm);
		System.out.println(tm.subMap("AUS","PAK"));
		System.out.println(tm.tailMap("PAK"));
		System.out.println(tm.firstKey());
		System.out.println(tm.lastKey());
		System.out.println(tm.keySet());
		System.out.println(tm.values());
	}
}
