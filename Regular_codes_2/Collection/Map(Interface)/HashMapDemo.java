import java.util.*;
class HashMapDemo{
	public static void main(String[] args){
		HashSet hs=new HashSet();
		hs.add("Pranav");
		hs.add("Mohit");
		hs.add("Vaibhav");
		hs.add("Vedant");
		hs.add("Sanket");
		hs.add("Rutik");
		System.out.println(hs);
		HashMap hm=new HashMap();
		hm.put("Pranav","Shiroli B.k.");
		hm.put("Mohit","Nagpur");
		hm.put("Vaibhav","Ahamadnagar");
		hm.put("Sanket","Otur");
		hm.put("Rutik","Pimpalgaon Joga");
		System.out.println(hm);
	}
}
