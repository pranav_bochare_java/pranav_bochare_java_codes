import java.util.*;
class Platform implements Comparable{
	String str=null;
	int foundYear=0;
	Platform(String str, int foundYear){
		this.str=str;
		this.foundYear=foundYear;
	}
	public int compareTo(Object obj){
		return this.foundYear-((Platform)obj).foundYear;
	}
}
class TreeMapDemo{
	public static void main(String[] args){
		TreeMap tm=new TreeMap();
		tm.put(new Platform("YT",2005),"Google");
		tm.put(new Platform("Inst",2010),"Meta");
		tm.put(new Platform("Face",2004),"meta");
		tm.put(new Platform("ChatGPT",2022),"OPenAI");
		System.out.println(tm);
	}
}


