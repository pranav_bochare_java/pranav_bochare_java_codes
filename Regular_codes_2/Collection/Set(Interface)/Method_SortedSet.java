import java.util.*;

class SortedSetDemo {
    public static void main(String[] args) {
        SortedSet<String> ss = new TreeSet<>();
        ss.add("Pranav");
        ss.add("Prathmesh");
        ss.add("Mohit");
        ss.add("Sanket");
        
        System.out.println(ss);
        System.out.println(ss.headSet("Pranav"));
        System.out.println(ss.tailSet("Pranav"));
        System.out.println(ss.subSet("Ashish", "Mohit"));
        System.out.println(ss.first());
        System.out.println(ss.last());
    }
}

