import java.io.*;
import java.util.*;

class PropertiesDemo {
    public static void main(String[] args) throws IOException {
        Properties obj = new Properties();
        FileInputStream fobj = new FileInputStream("friends.properties");
        obj.load(fobj);
        fobj.close();

        String name = obj.getProperty("Ashish");
        System.out.println(name);

        obj.setProperty("Shashi", "Binecaps");

        FileOutputStream outobj = new FileOutputStream("friends.properties");
        obj.store(outobj, "Updated by Shashi");
        outobj.close();
    }
}

