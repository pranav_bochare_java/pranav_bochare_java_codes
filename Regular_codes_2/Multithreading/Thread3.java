class Mythread extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
	public void start(){
		System.out.println("In my thread Start");
		run();
	}
}
class ThreadDemo{
	public static void main(String[] args){
		Mythread obj=new Mythread();
		obj.start();
		System.out.println(Thread.currentThread().getName());
	}
}
