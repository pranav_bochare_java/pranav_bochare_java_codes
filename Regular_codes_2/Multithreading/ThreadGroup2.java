class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){
		ThreadGroup pthreadGp=new ThreadGroup("C2W");
		Mythread obj1=new Mythread(pthreadGp,"c");
		Mythread obj2=new Mythread(pthreadGp,"Java");
		Mythread obj3=new Mythread(pthreadGp,"Python");
		obj1.start();
		obj2.start();
		obj3.start();
	}
}
