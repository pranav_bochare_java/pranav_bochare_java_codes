class Mythread extends Thread{
	Mythread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup pThreadGP=new ThreadGroup("India");
                Mythread t1=new Mythread(pThreadGP,"Maharastra");
		Mythread t2=new Mythread(pThreadGP,"Gujrat");
		t1.start();
		t2.start();

		ThreadGroup cThreadGP=new ThreadGroup("Pakistan");
		Mythread t3=new Mythread(cThreadGP,"Karachi");
		Mythread t4=new Mythread(cThreadGP,"Lahore");
		t3.start();
		t4.start();

		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}
}

		

