import java.util.concurrent.*;
class Mythread implements Runnable{
	int num;
	Mythread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+"StartThread:"  +"Task number" +num);
		dailyTask();
		System.out.println(Thread.currentThread()+"End Thread:" +"Task number" +num);
	}
	void dailyTask(){
		try{
			Thread.sleep(8000);
		}catch(InterruptedException ie){
		}
	}
}
class ThreadPoolDemo{
	public static void main(String[] args){
		ExecutorService ser=Executors.newCachedThreadPool();
		for (int i=0;i<=6;i++){
			Mythread obj=new Mythread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
