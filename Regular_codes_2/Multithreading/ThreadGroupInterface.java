class Mythread implements Runnable {
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String[] args)throws InterruptedException{
		ThreadGroup pThreadGP=new ThreadGroup("India");
		Mythread obj1=new Mythread();
		Mythread obj2=new Mythread();
		Thread t1=new Thread(pThreadGP,obj1,"Maharastra");
		Thread t2=new Thread(pThreadGP,obj2,"Goa");
		t1.start();
		t2.start();
	}
}

