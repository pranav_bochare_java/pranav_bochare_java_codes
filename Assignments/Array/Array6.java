import java.util.*;
class EvenOddCount{
	public static void main(String[] args){
		System.out.println("Enter Array Size");
		Scanner sc= new Scanner(System.in);
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count1=0;
		int count2=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2 == 0){
				count1++;
			}
			else{
				count2++;
			}
		}
		System.out.println("Even count = "+count1);
		System.out.println("Odd count ="+count2);
	}
}

	      	       
