import java.util.*;
class ArrayDemo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of Array");
		int size=sc.nextInt();
		int count=0;
		int arr[]=new int[size];
		System.out.println("Enter Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0){
				count++;
			}
		}
		System.out.println("Even Count="+count);
	}
}
