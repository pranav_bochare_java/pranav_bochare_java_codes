import java.io.*;
class AD{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Size of Array");
		int size =Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the Elements in Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int esum=0;
		int osum=0;
		for(int i=0; i<arr.length;i++){
			if(arr[i]%2==0){
				esum=esum+arr[i];
			}
			else{
				osum=osum+arr[i];
			}
		}
		System.out.println("The sum of Even Numbers are"+" "+esum);
		System.out.println("The sum of Odd Numbers are"+" "+osum);
	}
}

