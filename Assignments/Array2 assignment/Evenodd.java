import java.io.*;
class Ac{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Size of Array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter the Elements in Array");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int ecount=0; 
		int ocount=0;
		for(int i=0; i<arr.length;i++){
			if(arr[i]%2==0){
				ecount++;
			}
			else{
				ocount++;
			}
		}
		System.out.println("The Even Count is"+ecount);
		System.out.println("The Odd Count is"+ocount);
	}
}
